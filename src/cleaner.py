from kafka import KafkaConsumer
from kafka import KafkaProducer
from kafka.structs import OffsetAndMetadata, TopicPartition
from ordered_set import OrderedSet
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords 
from nltk.stem.porter import PorterStemmer
from autocorrect import Speller
from simplemma import text_lemmatizer
import os
import pandas as pd
import numpy as np
import json
import re
import nltk
import simplemma
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

nltk.download('stopwords')
nltk.download('punkt')

def load_datasets():
    stop_words_set = stopwords.words('italian')
    stemmer = PorterStemmer()
    spell = Speller(lang='it')
    langdata = simplemma.load_data('it')
    with open(os.getcwd() + '/src/data/ITGivenFemale.json') as f:
        data_female = json.load(f)

    with open(os.getcwd() + '/src/data/ITGivenMale.json') as f:
        data_male = json.load(f)

    data_names = list()
    for i in range(len(data_female)):
        data_names.append(str(data_female[i]['name']))
    for i in range(len(data_male)):
        data_names.append(str(data_male[i]['name']))
    for i in range(len(data_names)):
        data_names[i] = data_names[i].lower()

    df_cognomi = pd.read_csv(os.getcwd() + '/src/data/cognomi.txt')
    df_cognomi['Cognome'] = df_cognomi['Cognome'].str.lower()
    df_cognomi = list(df_cognomi['Cognome'])

    df_comuni = pd.read_csv(os.getcwd() + '/src/data/Elenco-comuni-italiani.csv', sep=';', encoding='latin1')
    df_comuni['Denominazione in italiano'] = df_comuni['Denominazione in italiano'].str.lower()
    df_comuni = list(df_comuni['Denominazione in italiano'])

    with open(os.getcwd() + '/src/data/vie_genova.json') as f:
        data_streets = json.load(f)
    df_streets = pd.DataFrame(data_streets['Risposta']['Strada'])
    df_streets['NOME_VIA'] = df_streets['NOME_VIA'].str.lower()
    corpus = ""
    for item in df_streets:
        corpus = corpus + " " + item
    corpus = OrderedSet(word_tokenize(corpus))
    corpus = list(corpus)
    df_streets = corpus

    return stop_words_set, stemmer, spell, langdata, data_names, df_comuni, df_cognomi, df_streets

print("Loading datasets...")
stop_words_set, stemmer, spell, langdata, data_names, df_comuni, df_cognomi, df_streets = load_datasets()

def json_serializer(data):
    return json.dumps(data).encode("utf-8")

def prepare_and_send_msg(msg_json, producer):
    if("categories" in msg_json):
        if(msg_json['version'] == 1):
            subject_description = clean_document(msg_json['subject'] + " " + msg_json['description'])
            post = {
                "subject_description": subject_description,
                "categoryId": msg_json['categories'][0]['id'],
                "parentCategoryId": msg_json['categories'][0]['parent'],
                "categoryName": msg_json['categories'][0]['name'],
                "status": msg_json['status'] 
            }
            logging.info(f'segnalazioni_clean = ' + str(post) + "\n")
            producer.send(os.environ['KAFKA_TOPIC_SEGNALAZIONI_CLEAN'], post)


def clean_document(document):
    document = re.sub('[^A-Za-z]', ' ', document)
    document = document.lower()

    tokenized_text = word_tokenize(document)
    tokenized_text = [w for w in tokenized_text if not w in stop_words_set]
    tokenized_text = [w for w in tokenized_text if not w in data_names]
    tokenized_text = [w for w in tokenized_text if not w in df_comuni]
    tokenized_text = [w for w in tokenized_text if not w in df_cognomi]
    tokenized_text = [w for w in tokenized_text if not w in df_streets]
    tokenized_text = [w for w in tokenized_text if not w in ['copia']]
    
    words = []
    for word in tokenized_text:
        words.append(stemmer.stem(" ".join(text_lemmatizer(spell(word), langdata))))
        
    cleaned_text = " ".join(words)
    document = cleaned_text
    return document

if __name__ == "__main__":
    consumer = KafkaConsumer(os.environ['KAFKA_TOPIC_SEGNALAZIONI_RAW'],
                             bootstrap_servers=[broker for broker in os.environ['KAFKA_BROKERS'].split(',')],
                             auto_offset_reset='earliest',
                             group_id=os.environ['KAFKA_CONSUMER_GROUP_ID'],
                             enable_auto_commit=False)
    producer = KafkaProducer(bootstrap_servers=[broker for broker in os.environ['KAFKA_BROKERS'].split(',')], 
                             value_serializer=json_serializer)
    
    print("starting the consumer")
    counter = 0
    for msg in consumer:
        counter+=1
        print("cleaner count: " + str(counter) + "\n")
        prepare_and_send_msg(json.loads(msg.value), producer)
        tp = TopicPartition(topic=msg.topic, partition=msg.partition)
        om = OffsetAndMetadata(offset=msg.offset+1, metadata='')
        try:
            consumer.commit(offsets={tp: om})
        except Exception:
            logging.exception('An error occurred.')