#!/bin/bash
set -e

brokers=${KAFKA_BROKERS:?'Missing required parameters'}
firstBroker=$(echo $brokers | cut -d',' -f1)
./src/wait-for-it.sh $firstBroker -s -t 120
python3 src/cleaner.py
