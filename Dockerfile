# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

ARG USERNAME=cleaner
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

EXPOSE 5001

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app

RUN mkdir src
RUN mkdir src/data

COPY /src/cleaner.py /app/src 
COPY /src/wait-for-it.sh /app/src
COPY /src/select-broker.sh /app/src
COPY /src/data/ITGivenFemale.json /app/src/data
COPY /src/data/ITGivenMale.json /app/src/data
COPY /src/data/cognomi.txt /app/src/data
COPY /src/data/Elenco-comuni-italiani.csv /app/src/data
COPY /src/data/vie_genova.json /app/src/data

# Give permission to -c to execute wait-for-it.sh
RUN sudo chmod +x ./src/wait-for-it.sh
RUN sudo chmod +x ./src/select-broker.sh

CMD ["bash", "./src/select-broker.sh"]
