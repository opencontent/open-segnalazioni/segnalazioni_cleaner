# segnalazioni_clener
## Descrizione
Questo componente si occupa di leggere da un dato kafk-topic il testo di tutte le segnalazioni presenti nel db di SegnalaCi, pulirle da tutte le informazioni non utili ai fini della fase di training di un modello e di scriverle successivamente su un altro kafka-topic