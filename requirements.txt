autocorrect==2.5.0
nltk==3.6.5
ordered_set==4.0.2
numpy==1.19.5
pandas==1.1.5
simplemma==0.4.0
kafka_python==2.0.2
#kafka==1.3.5
